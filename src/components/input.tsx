import * as React from 'react';

interface InputProps {
    pattern: string;
    isFetch: boolean;
    updatePattern: (pattern: string) => void;
    fetchUsers: () => void;
}

interface InputState {
    value: string;
}

class Input extends React.Component<InputProps, InputState> {

    constructor(props: InputProps) {
        super(props);
        this.state = { value: this.props.pattern };
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event: React.FormEvent<HTMLInputElement>) {
        const value: string = event.currentTarget.value;
        this.setState({ value }, this.applyChange);
    }
    
    applyChange() {
        this.props.updatePattern(this.state.value);
        if (this.state.value) {         // Получать пользователей будем только для непустого шаблона
            this.props.fetchUsers();
        }
    }
    
    componentWillReceiveProps(nextProps: InputProps) {
        this.setState({ value: nextProps.pattern });
    }
    
    render() {
        return (
            <div className="input">
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                {
                    this.props.isFetch &&
                        <img src={'/img/loader.gif'} role="presentation" />
                }
            </div>
        );
    }
}

export default Input;