import * as React from 'react';
import Select from '../containers/select';

class App extends React.Component<null, null> {
    render() {
        return (
            <div>
                <Select id="s1" />
            </div>
        );
    }
}

export default App;
