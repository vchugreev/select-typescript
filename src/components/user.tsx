import * as React from 'react';

interface UserProps {
    userId: number;
    userName: string;
}

class User extends React.Component<UserProps, null> {
    render() {
        return (
            <div data-key={this.props.userId} className="user-list-item">
                {this.props.userName}
            </div>
        );
    }
}

export default User;