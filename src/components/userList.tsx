import * as React from 'react';

interface UserListProps {
    selectUser: (userKey: number) => void;
}

class UserList extends React.Component<UserListProps, null> {

    constructor(props: UserListProps) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(event: React.FormEvent<HTMLDivElement>) {
        const target = event.target as HTMLDivElement;
        this.props.selectUser(Number(target.dataset.key));
    }
    
    render() {
        return (
            <div className="user-list" onClick={this.handleClick}>
                {this.props.children}
            </div>
        );
    }
}

export default UserList;
