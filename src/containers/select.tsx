import * as React from 'react';
import { connect } from 'react-redux';
import { updatePattern, fetchUsers, selectUser } from '../actions';
import Input from '../components/input';
import UserList from '../components/userList';
import User from '../components/user';
import IUser from '../interfaces/user';
import Store from '../interfaces/store';

interface SelectProps {
    id: string;
}

interface StateProps {
    pattern: string;
    isFetch: boolean;
    users: Array<IUser>;
}

interface DispatchProps {
    updatePattern: (pattern: string) => void;
    fetchUsers: () => void;
    selectUser: (userKey: number) => void;
}

class Select extends React.Component<SelectProps & DispatchProps & StateProps, {}> {

    render() {
        return (
            <div className="select">
                <Input
                    pattern={this.props.pattern}
                    updatePattern={this.props.updatePattern}
                    fetchUsers={this.props.fetchUsers}
                    isFetch={this.props.isFetch}
                />
                
                <UserList selectUser={this.props.selectUser}>
                    {this.props.users.map(user => {
                        return (
                            <User key={user.id} userId={user.id} userName={user.name} />
                        );
                    })}
                </UserList>
            </div>
        );
    }
}

const mapStateToProps = function(store: Store, ownProps: SelectProps): StateProps {
    const state = store[ownProps.id];
    return {
        isFetch: state ? Boolean(state.idFetch) : false,
        pattern: state ? state.pattern : '',
        users: state ? state.users : []
    };
};

const mapDispatchToProps = function(dispatch: Function, ownProps: SelectProps): DispatchProps {
    return {
        updatePattern: (pattern: string) => {
            dispatch(updatePattern(ownProps.id, pattern));
        },
        fetchUsers: () => {
            dispatch(fetchUsers(ownProps.id));
        },
        selectUser: (userKey: number) => {
            dispatch(selectUser(ownProps.id, userKey));
        }
    };
};

export default connect<{}, {}, SelectProps>(mapStateToProps, mapDispatchToProps)(Select);