import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import * as createLogger from 'redux-logger';
import reducer from './reducers';
import App from './components/app';

const logger = createLogger();
const middleware = (process.env.NODE_ENV === 'production') ? [ thunk ] : [ thunk, logger ];

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
