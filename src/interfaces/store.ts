import User from "../interfaces/user";

interface Select {
    pattern: string;
    users: Array<User>;
    idFetch?: string;
}

type Store = Array<Select>;

export default Store;


