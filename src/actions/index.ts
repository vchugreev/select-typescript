import * as _ from 'lodash';
import User from '../interfaces/user';

export type UPDATE_PATTERN = 'UPDATE_PATTERN';
export const UPDATE_PATTERN: UPDATE_PATTERN = 'UPDATE_PATTERN';

export type REQUEST_USERS = 'REQUEST_USERS';
export const REQUEST_USERS: REQUEST_USERS = 'REQUEST_USERS';

export type RECEIVE_USERS = 'RECEIVE_USERS';
export const RECEIVE_USERS: RECEIVE_USERS = 'RECEIVE_USERS';

export type SELECT_USER = 'SELECT_USER';
export const SELECT_USER: SELECT_USER = 'SELECT_USER';

export type UpdatePatternAction = {
    type: UPDATE_PATTERN,
    id: string,
    pattern: string
};

export type RequestUserAction = {
    type: REQUEST_USERS,
    id: string,
    idFetch: string
};

export type ReceiveUserAction = {
    type: RECEIVE_USERS,
    id: string,
    idFetch: string,
    users: Array<User>
};

export type SelectUserAction = {
    type: SELECT_USER,
    id: string,
    userKey: number
};

// Исключительно для эмуляции получения данных!
let data: Array<User> = [
    {'id': 1, 'name': 'Tom Hanks'},
    {'id': 2, 'name': 'Bruce Willis'},
    {'id': 3, 'name': 'Tom Cruise'},
    {'id': 4, 'name': 'Matt Damon'},
    {'id': 5, 'name': 'Michael Caine'},
    {'id': 6, 'name': 'Samuel L. Jackson'},
    {'id': 7, 'name': 'Tommy Lee Jones'},
    {'id': 8, 'name': 'Gary Oldman'},
    {'id': 9, 'name': 'Will Smith'},
    {'id': 10, 'name': 'Harrison Ford'},
    {'id': 11, 'name': 'Jack Nicholson'},
    {'id': 12, 'name': 'Robert De Niro'},
    {'id': 13, 'name': 'Al Pacino'},
    {'id': 14, 'name': 'Anthony Hopkins'},
    {'id': 15, 'name': 'Clint Eastwood'}
];

/**
 *
 * @param id - идентификатор корневого элемента (селекта)
 * @param pattern - шаблон поиска
 */
export const updatePattern = (id: string, pattern: string): UpdatePatternAction => ({
    type: UPDATE_PATTERN,
    id,
    pattern
});

/**
 *
 * @param id - идентификатор корневого элемента (селекта)
 * @param idFetch - идентификатор запроса на получения данных (дает возможность проигнорировать неактуальный запрос)
 */
export const requestUsers = (id: string, idFetch: string): RequestUserAction => ({
    type: REQUEST_USERS,
    id,
    idFetch
});

export const receiveUsers = (id: string, users: Array<User>, idFetch: string): ReceiveUserAction => ({
    type: RECEIVE_USERS,
    id,
    users,
    idFetch
});

export const fetchUsers = (id: string) => (dispatch: Function) => {
    
    // Цикл получения данных включает в себя 3 этапа: 1) requestUsers, 2) fetchUsers, 3) receiveUsers
    // Он может быть прераван, если инициирован новый запрос на получение данных.
    // Для того, чтобы иметь возможность его прервать мы сохраняем idFetch
    // (логика idFetch обрабатывается в reducers\index.js на RECEIVE_USERS)
    
    const idFetch: string = _.uniqueId();
    dispatch(requestUsers(id, idFetch));
    
    // Иммитация получения данных
    return new Promise(function(resolve: Function) {
        setTimeout(resolve, 500, data);
    })
    .then((users: Array<User>) => dispatch(receiveUsers(id, users, idFetch)));
};

export const selectUser = (id: string, userKey: number): SelectUserAction => ({
    type: SELECT_USER,
    id,
    userKey,
});
